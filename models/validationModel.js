class ValidatorProduct {
  constructor() {
    this.isInputEmpty = (idTarget, idError) => {
      let valueTarget = document.getElementById(idTarget).value;

      if (!valueTarget) {
        document.getElementById(idError).innerText = "Không được để rỗng !!!";
        return false;
      } else {
        document.getElementById(idError).innerText = "";
        return true;
      }
    };
  }
}
