// bật, tắt loading
const turnOnLoading = function () {
  document.getElementById("loading").style.display = "flex";
};

const turnOffLoading = function () {
  document.getElementById("loading").style.display = "none";
};

let productList = [];

// lấy danh sách sản phẩm
const fetchProducts = () => {
  turnOnLoading();
  productService
    .getList()
    .then((res) => {
      //   console.log("res: ", res);
      turnOffLoading();
      productList = res.data;
      renderProducts();
    })
    .catch((err) => {
      console.log("err: ", err);
      turnOffLoading();
    });
};

fetchProducts();

// hiển thị danh sách sản phẩm
const renderProducts = () => {
  let contentHTML = "";

  for (let product of productList) {
    contentHTML += /*html*/ `<tr>

    <td>${product.id * 1 + 1}</td>
    <td>${product.name}</td>
    <td>${parseInt(product.price).toLocaleString()}</td>
    <td>${product.screen}</td>
    <td>${product.backCamera}</td>
    <td>${product.frontCamera}</td>
    <td><img src=${product.img} alt=${
      product.name
    } style="width:100px;height:100px"></td>
    <td>${product.desc}</td>
    <td>${product.type == 0 ? "Iphone" : "Samsung"}</td>
    <td>
        <button
        onclick="editProduct(${product.id})"
        class="btn btn-primary m-1">Sửa</button>
        <button
        onclick="deleteProduct(${product.id})"
        class="btn btn-danger m-1">Xoá</button>
    </td>
    
    </tr>`;
  }
  document.getElementById("tblDanhSachSanPham").innerHTML = contentHTML;
};

let validatorProduct = new ValidatorProduct();

// thêm sản phẩm
const addProduct = () => {
  let newProduct = getProductInfo();
  turnOnLoading();

  // kiểm tra rỗng
  let isValidName = validatorProduct.isInputEmpty("tenSP", "tbTenSP");
  let isValidPrice = validatorProduct.isInputEmpty("giaBan", "tbGiaBan");
  let isValidScreen = validatorProduct.isInputEmpty("manHinh", "tbManHinh");
  let isValidBackCamera = validatorProduct.isInputEmpty(
    "cameraSau",
    "tbCameraSau"
  );
  let isValidFrontCamera = validatorProduct.isInputEmpty(
    "cameraTruoc",
    "tbCameraTruoc"
  );
  let isValidImg = validatorProduct.isInputEmpty("hinhAnh", "tbHinhAnh");
  let isValidDesc = validatorProduct.isInputEmpty("moTa", "tbMoTa");

  let isValid =
    isValidName &&
    isValidPrice &&
    isValidScreen &&
    isValidBackCamera &&
    isValidFrontCamera &&
    isValidImg &&
    isValidDesc;

  if (isValid) {
    productService
      .postProduct(newProduct)
      .then((res) => {
        //   console.log("res: ", res);
        turnOffLoading();
        $("#myModal").modal("hide");
        fetchProducts();
      })
      .catch((err) => {
        console.log("err: ", err);
        turnOffLoading();
      });
  }
};

// xoá sản phẩm
const deleteProduct = (id) => {
  turnOnLoading();
  productService
    .delProduct(id)
    .then((res) => {
      //   console.log("res: ", res);
      turnOffLoading();
      fetchProducts();
    })
    .catch((err) => {
      console.log("err: ", err);
      turnOffLoading();
    });
};

// chỉnh sửa sản phẩm
const editProduct = (id) => {
  document.getElementById("btnThemSanPham").click();
  document.getElementById("btn-add").style.display = "none";
  document.getElementById("btn-update").style.display = "block";

  productService
    .getProduct(id)
    .then((res) => {
      //   console.log("res: ", res);
      document.querySelector("#ID-product span").innerHTML = res.data.id;
      showProductInfo(res.data);
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};

// cập nhật sản phẩm

const updateProduct = () => {
  let updatedProduct = getProductInfo();
  turnOnLoading();

  let productID = document.querySelector("#ID-product span").innerHTML;
  productService
    .putProduct(productID, updatedProduct)
    .then((res) => {
      //   console.log("res: ", res);
      turnOffLoading();
      $("#myModal").modal("hide");
      fetchProducts();
    })
    .catch((err) => {
      console.log("err: ", err);
      turnOffLoading();
    });
};

// reset btnThemSanPham
document.getElementById("btnThemSanPham").addEventListener("click", () => {
  document.getElementById("form-product").reset();
  document.getElementById("btn-add").style.display = "block";
  document.getElementById("btn-update").style.display = "none";
});

// hiển thị danh sách sản phẩm đang tìm kiếm
const renderSearchedProducts = (searchedList) => {
  let contentHTML = "";
  for (let key of searchedList) {
    contentHTML += /*html*/ `<tr>

    <td>${key.id * 1 + 1}</td>
    <td>${key.name}</td>
    <td>${key.price}</td>
    <td>${key.screen}</td>
    <td>${key.backCamera}</td>
    <td>${key.frontCamera}</td>
    <td><img src=${key.img} alt=${
      key.name
    } style="width:100px;height:100px"></td>
    <td>${key.desc}</td>
    <td>${key.type == 0 ? "Iphone" : "Samsung"}</td>
    <td>
        <button
        onclick="editProduct(${key.id})"
        class="btn btn-primary m-1">Sửa</button>
        <button
        onclick="deleteProduct(${key.id})"
        class="btn btn-danger m-1">Xoá</button>
    </td>

    </tr>`;
  }
  document.getElementById("tblDanhSachSanPham").innerHTML = contentHTML;
};

// tìm kiếm sản phẩm
document.getElementById("btnTimKiem").addEventListener("click", () => {
  let keyInput = document.getElementById("tuKhoa").value.trim().toLowerCase();

  console.log(keyInput);

  let searchProduct = () => {
    return keyInput == "iphone"
      ? productList.filter((product) => product.type == 0)
      : productList.filter((product) => product.type == 1);
  };
  console.log(searchProduct());
  renderSearchedProducts(searchProduct());
  document.getElementById("tuKhoa").value = "";
});

// quay về trang admin sau khi kết thúc tìm kiếm
document.getElementById("btnQuayVe").addEventListener("click", () => {
  fetchProducts();
});
